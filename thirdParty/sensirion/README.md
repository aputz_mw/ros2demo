# Third party libraries by senserion

Directory Layout:

    |-embedded-sps      - The i2c driver for Linux/Arduino
    |-embedded-uart-sps - The Uart driver for LInux/Arduino

Attention: These two directories are sub-repositories. Hence they will be empty on initial checkout and only be populated as soon as the sub-repositories are initialized and updated.

More information on Linux/Arduino builds can be found [here](https://developer.sensirion.com/platforms/generic-linux-support/).


## SPS30 Particle Matter Sensor

### UART connection to Linuc PC:

The SPS30 can be connected to any PC via a special Uart to USB cable.
After connecting the cable to a Linux PC (Ubuntu 18.04), the kernel reports a new FTDI USB serial connector attached to the UB port (here ttyUSB0):

    $ dmesg
    [768116.309566] usb 3-1.3: new full-speed USB device number 116 using xhci_hcd
    [768116.429756] usb 3-1.3: New USB device found, idVendor=0403, idProduct=6001
    [768116.429760] usb 3-1.3: New USB device strings: Mfr=1, Product=2, SerialNumber=3
    [768116.429763] usb 3-1.3: Product: Sensirion UART-USB Cable
    [768116.429766] usb 3-1.3: Manufacturer: Sensirion AG
    [768116.429769] usb 3-1.3: SerialNumber: FT303TWQ
    [768116.482230] usbcore: registered new interface driver usbserial_generic
    [768116.482249] usbserial: USB Serial support registered for generic
    [768116.484982] usbcore: registered new interface driver ftdi_sio
    [768116.484996] usbserial: USB Serial support registered for FTDI USB Serial Device
    [768116.485177] ftdi_sio 3-1.3:1.0: FTDI USB Serial Device converter detected
    [768116.485212] usb 3-1.3: Detected FT232RL
    [768116.485579] usb 3-1.3: FTDI USB Serial Device converter now attached to ttyUSB0

Now check the connection type

    $ setserial -g /dev/ttyUSB0
    /dev/ttyUSB0: Permission denied

    # Or if root permissions are required:
    $ sudo setserial -g /dev/ttyUSB0
    /dev/ttyUSB0, UART: unknown, Port: 0x0000, IRQ: 0


### Compilation of the source code (UART)

**Step1** - Make the release directory following the [README.md](embedded-uart-sps/README.md)

Note: I had to patch the Makefile to not complain about sub-repositories. Make sure you have the patch applied.

    $ cd thirdParty/sensirion/embedded-uart-sps
    $ make release
    $ cd release/sps30-uart

**Step 2** - Building the driver

1. Step into your desired directory (e.g.: `release/sps30-uart`)
2. Adjust sensirion\_arch\_config.h if the `<stdint.h>` header is not available
3. Implement necessary functions in `*_implementation.c`
4. make



