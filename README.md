# Sensor testbed application

This repository contains all source code to run a sensor testbed for the following sensors:

* [Sensirion SPS30](https://www.sensirion.com/en/environmental-sensors/particulate-matter-sensors-pm25/) Particle counter

## Repository Structure

| Directory | Purpose |
|-----------|---------|
| thirdparty     | Third party libraries |
|   -> [sensirion](thirdParty/sensirion/README.md) | sensirion sensor source code (git submodule) |

### Repository Checkout

    $ git clone git@bitbucket.org:mw_active/2019hm-iot.git
    $ git submodule init
    $ git submodule update --init --recursive

    # Apply Patches
    $ cd thirdParty/sensirion/embedded-uart-sps
    $ git apply ../01-repo.patch

