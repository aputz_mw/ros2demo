#include "sensirion/sps30.hpp"

// STD
#include <string>

namespace sensirion {

SPS30Node::SPS30Node(ros::NodeHandle& nodeHandle)
    : nodeHandle_(nodeHandle)
{
  if (!readParameters()) {
    ROS_ERROR("Could not read parameters.");
    ros::requestShutdown();
  }
  subscriber_ = nodeHandle_.subscribe(subscriberTopic_, 1,
                                      &SPS30Node::topicCallback, this);
  serviceServer_ = nodeHandle_.advertiseService("get_average",
                                                &SPS30Node::serviceCallback, this);
  ROS_INFO("Successfully launched node.");
}

SPS30Node::~SPS30Node()
{
}

bool SPS30Node::readParameters()
{
  if (!nodeHandle_.getParam("subscriber_topic", subscriberTopic_)) return false;
  return true;
}

void SPS30Node::topicCallback(const sensor_msgs::Temperature& message)
{
  algorithm_.addData(message.temperature);
}

bool SPS30Node::serviceCallback(std_srvs::Trigger::Request& request,
                                         std_srvs::Trigger::Response& response)
{
  response.success = true;
  response.message = "The average is " + std::to_string(algorithm_.getAverage());
  return true;
}






} /* namespace */


int main(int argc, char** argv)
{
  ros::init(argc, argv, "ros_package_template");
  ros::NodeHandle nodeHandle("~");

  sensirion::SPS30Node SPS30NodeInstance(nodeHandle);

  ros::spin();
  return 0;
}