#include <ros/ros.h>
#include "sensirion/sps30.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ros_package_template");
  ros::NodeHandle nodeHandle("~");

  sensirion::SPS30Node SPS30NodeInstance(nodeHandle);

  ros::spin();
  return 0;
}
